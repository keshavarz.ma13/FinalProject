package game.sample.ball;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

public class GameWin extends ShotBox {


    /**
     * Fields
     */
    JFrame winingFrame = new JFrame("Edit Map");
    ImageIcon backImage = new ImageIcon("Resources\\Images\\youWin.png");
    JLabel background = new JLabel(backImage);
    JButton exit = new JButton("Exit");
    JButton playAgain = new JButton("Play again");

    /**
     * Constructor
     *
     * @param x
     * @param y
     */
    public GameWin(int x, int y) {
        super(x, y);
        try {
            image = ImageIO.read(new File("Resources\\Images\\nextLevel.png"));
        } catch (IOException e) {
        }
    }

    /**
     * This method is for check collision of player's tank
     *
     * @param x is x location of player tank
     * @param y is y location of player tank
     */
    public void colision(int x, int y) {
        Rectangle r = new Rectangle(x, y, 180, 180);
        Rectangle p = new Rectangle(locX, locY, 100, 100);
        if (p.intersects(r)) {

            GameWin.ActionHandler actionHandler = new GameWin.ActionHandler();
            winingFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
            winingFrame.setUndecorated(true);
            winingFrame.setBackground(Color.BLACK);
            winingFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            exit.setBounds(new Rectangle(1550 - 700, 800, 80, 50));
            playAgain.setBounds(new Rectangle(1650 - 700, 800, 120, 50));
            exit.addActionListener(actionHandler);
            playAgain.addActionListener(actionHandler);
            winingFrame.add(exit);
            winingFrame.add(playAgain);
            winingFrame.add(background);

            winingFrame.setVisible(true);
        }
    }


    class ActionHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            for (Tank tank : Tank.enemyTanks)
                tank.isLife = false;
            for (Beetle beetle : Beetle.beetles)
                beetle.isLife = false;
            for (Gunshot gunshot : Gunshot.getShots()) {
                gunshot.locationX = 10000;
            }
            for (ShotBox shotbox : ShotBox.ShotBoxes) {
                shotbox.isVisible = false;

            }
            GameFrame.numberOfMap--;
            GameState.locY = 100;
            GameState.locX = 100;

            if (e.getSource().equals(exit)) {
                winingFrame.dispatchEvent(new WindowEvent(winingFrame, WindowEvent.WINDOW_CLOSING));
            }
            if (e.getSource().equals(playAgain)) {

                if (GameFrame.numberOfMap == 1)
                    GameMap.readMap("map.txt");
                else
                    GameMap.readMap("map2.txt");
                GameFrame frame = new GameFrame("Simple Ball !");
                frame.setLocationRelativeTo(null); // put frame at center of screen
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
                frame.initBufferStrategy();
                // Create and execute the game-loop
                GameLoop game = new GameLoop(frame);
                game.init();
                ThreadPool.execute(game);
                // and the game starts ...
            }
        }
    }

}

package game.sample.ball;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class GameMap {

    /**
     * fields
     */
    static ObjectBlock[][] map = new ObjectBlock[20][25];
    //labels for edit map
    static JTextField[][] mapLabel = new JTextField[20][25];


    /**
     * create buffered image for object block
     */
    public static BufferedImage soil = null;
    public static BufferedImage softWall = null;
    public static BufferedImage hardWall = null;
    public static BufferedImage teazel = null;
    public static BufferedImage plant = null;


    /**
     * Read the image of
     */
    static {
        try {
            soil = ImageIO.read(new File("Resources\\Images\\Area.png"));
            softWall = ImageIO.read(new File("Resources\\Images\\softWall.png"));
            hardWall = ImageIO.read(new File("Resources\\Images\\hardWall.png"));
            teazel = ImageIO.read(new File("Resources\\Images\\teazel2.png"));
            plant = ImageIO.read(new File("Resources\\Images\\plant.png"));
        } catch (IOException e) {
        }
    }

    /**
     * This method is for read the map
     * @param path is path of map file
     */
    public static void readMap(String path) {

        String FILENAME = path ;
        int n = 0;


        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {

                if (sCurrentLine.equals("s")) {
                    mapLabel[n%20][n/20]= new JTextField("s");
                    ObjectBlock object = new ObjectBlock(1, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\Area.png");
                    map[n % 20][n / 20] = object;
                } else if (sCurrentLine.equals("hw")) {
                    mapLabel[n%20][n/20]= new JTextField("hw");
                    ObjectBlock object = new ObjectBlock(2, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\hardWall.png");
                    map[n % 20][n / 20] = object;
                } else if (sCurrentLine.equals("sw")) {
                    mapLabel[n%20][n/20]= new JTextField("sw");
                    ObjectBlock object = new ObjectBlock(3, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\softWall.png");
                    map[n % 20][n / 20] = object;
                } else if (sCurrentLine.equals("t")) {
                    mapLabel[n%20][n/20]= new JTextField("t");
                    ObjectBlock object = new ObjectBlock(4, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\teazel2.png");
                    map[n % 20][n / 20] = object;
                } else if (sCurrentLine.equals("p")) {
                    mapLabel[n%20][n/20]= new JTextField("p");
                    ObjectBlock object = new ObjectBlock(5, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\plant.png");
                    map[n % 20][n / 20] = object;
                }
                n++;

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * This method is for update all of map
     * @param u is for up
     * @param d is for down
     */
    public static void updateMap(int u, int d) {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 25; j++) {
                map[i][j].y = map[i][j].y + u * 4 - d * 4;

            }
        }
    }

    /**
     * This method is for show the edited map
     */
    public static void editMap (){
        new EditMap().show();
    }


}

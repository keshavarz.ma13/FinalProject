package game.sample.ball;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class Gunshot implements Serializable {

    /**
     * Fields
     */
    int locationX, locationY;
    private BufferedImage img = null;
    private double angle;
    private static ArrayList<Gunshot> shots = new ArrayList<Gunshot>();
    boolean fromEnemy;
    boolean isMachineGun;

    /**
     * Constructor
     *
     * @param locationX    is x location of gun shot
     * @param locationY    is y location of gun shot
     * @param angle
     * @param fromEnemy    is from enemy or player tank
     * @param isMachineGun is machine gun or no
     */
    public Gunshot(int locationX, int locationY, double angle, boolean fromEnemy, boolean isMachineGun) {
        this.angle = -angle + Math.toRadians(90);
        this.locationX = locationX - 10 + (int) (80 * Math.cos(angle));
        this.locationY = locationY + (int) (80 * Math.sin(angle));
        try {
            if (!fromEnemy)
                img = ImageIO.read(new File("Resources\\Images\\HeavyBullet.png"));
            else if (fromEnemy)
                img = ImageIO.read(new File("Resources\\Images\\enemyBullet.png"));
            if (!fromEnemy && isMachineGun)
                img = ImageIO.read(new File("Resources\\Images\\enemyBullet.png"));
        } catch (IOException e) {
        }
        this.fromEnemy = fromEnemy;
        this.isMachineGun = isMachineGun;

    }


    /**
     * This method is update gun shots
     */
    public static void update() {
        if (shots.size() <= 30) {
            for (Gunshot shot : shots) {
                if (shot.fromEnemy) {
                    shot.locationX += 20 * Math.sin(shot.angle);
                    shot.locationY += 20 * Math.cos(shot.angle);
                } else {
                    shot.locationX += 20 * GameState.weaponPower * Math.sin(shot.angle);
                    shot.locationY += 20 * GameState.weaponPower * Math.cos(shot.angle);
                }

                boolean flag = false;
                for (int i = 0; i < 20; i++) {
                    for (int j = 0; j < 25; j++) {
                        if (shot.collide(GameMap.map[i][j].image, GameMap.map[i][j].x, GameMap.map[i][j].y)) {
                            if (GameMap.map[i][j].type == 2) {
                                if (!shot.fromEnemy)
                                    new SoundPlayer(false, "Resources\\Sounds\\recosh.wav").execute();
                                shot.locationY = 10000;
                                shot.locationX = 10000;
                            } else if (GameMap.map[i][j].type == 3) {
                                shot.locationY = 10000;
                                shot.locationX = 10000;
                                new SoundPlayer(false, "Resources\\Sounds\\softwall.wav").execute();
                                if (GameState.isMachineGun)
                                    GameMap.map[i][j].softWallLevel--;
                                else
                                    GameMap.map[i][j].softWallLevel -= 5;
                            }
                        }
                    }
                }


            }
        } else if (shots.size() > 30)
            for (int i = shots.size() - 30; i < shots.size(); i++) {
                shots.get(i).locationX += 20 * GameState.weaponPower * Math.sin(shots.get(i).angle);
                shots.get(i).locationY += 20 * GameState.weaponPower * Math.cos(shots.get(i).angle);
                for (int k = 0; k < 20; k++) {
                    for (int j = 0; j < 25; j++) {
                        if (shots.get(i).collide(GameMap.map[k][j].image, GameMap.map[k][j].x, GameMap.map[k][j].y)) {
                            if (GameMap.map[k][j].type == 2) {
                                shots.get(i).locationY = 10000;
                                shots.get(i).locationX = 10000;
                            } else if (GameMap.map[k][j].type == 3) {
                                shots.get(i).locationY = 10000;
                                shots.get(i).locationX = 10000;
                                new SoundPlayer(false, "Resources\\Sounds\\softwall.wav").execute();

                                if (GameState.isMachineGun) {
                                    GameMap.map[k][j].softWallLevel -= 1;
                                } else {
                                    GameMap.map[k][j].softWallLevel -= 5;
                                }

                            }
                        }
                    }
                }
            }

    }

    /**
     * get gun shots
     *
     * @return shots
     */
    public static ArrayList<Gunshot> getShots() {
        return shots;
    }

    /**
     * get angles
     *
     * @return anglw
     */
    public double getAngle() {
        return angle;
    }

    public BufferedImage getImg() {
        return img;
    }

    /**
     * This method is for check collision of player's tank with gun shot
     *
     * @param image
     * @param x
     * @param y
     * @return
     */
    public boolean collide(BufferedImage image, int x, int y) {

        Rectangle r = new Rectangle(locationX, locationY, getImg().getWidth(), getImg().getHeight());
        Rectangle p = new Rectangle(x, y, 100, 100);

        // Assuming there is an intersect method, otherwise just handcompare the values
        if (p.intersects(r)) {
            return true;
        } else
            return false;
    }
}

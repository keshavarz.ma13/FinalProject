package game.sample.ball;

public class RocketFixedEnemy extends FixedEnemy {

    /**
     * field
     */
    public int number;

    /**
     * constructor
     *
     * @param x        is x location of rocket fixed enemy
     * @param y        is y location of rocket fixed enemy
     * @param imgPath  is path of the body of rocket fixed enemy
     * @param imgPath2 is path of the gun of rocket fixed enemy
     */
    public RocketFixedEnemy(int x, int y, String imgPath, String imgPath2) {
        super(x, y, imgPath, imgPath2);
    }


    /**
     * Find angel and shutting rocket fixed enemy
     *
     * @param x is x location of player tank
     * @param y is y location of player tank
     */
    @Override
    public void move(int x, int y) {
        lifeOfEnemyTank -= collisionShots();
        if (lifeOfEnemyTank <= 0) {
            new SoundPlayer(false, "Resources\\Sounds\\enemydestroyed.wav").execute();
            isLife = false;
        }
        if (number % 60 == 0 && isLife) {
            if (Math.abs(locX - x) < 1000 && Math.abs(locY - y) < 1000 && locY > 0) {
                new SoundPlayer(false, "Resources\\Sounds\\cannon.wav").execute();
                Beetle.beetles.add(new Rocket(locX, locY, "Resources\\Images\\enemyRocket.png"));
            }
        }

        if (x + 86 > locX + 50)
            angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(0);

        else if (x + 86 < locX + 50) {
            angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(180);
        }
        number++;
    }

}

package game.sample.ball;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class LevelUpdater extends ShotBox {

    /**
     * Constructor
     *
     * @param x is x location of level updater
     * @param y is y location of level updater
     */
    public LevelUpdater(int x, int y) {
        super(x, y);
        try {
            image = ImageIO.read(new File("Resources\\Images\\nextLevel.png"));
        } catch (IOException e) {
        }
    }

    /**
     * This method is for check collision of player's tank with level updater
     *
     * @param x is x location of level updater
     * @param y is y location of level updater
     */
    public void colision(int x, int y) {

        Rectangle r = new Rectangle(x, y, 180, 180);
        Rectangle p = new Rectangle(locX, locY, 100, 100);
        if (p.intersects(r)) {
            new SoundPlayer(false, "Resources\\Sounds\\repair.wav").execute();
            GameFrame.numberOfMap++;
            GameLoop.state.locX = 100;
            GameLoop.state.locY = 200;

            for (Tank tank : Tank.enemyTanks)
                tank.isLife = false;
            for (Beetle beetle : Beetle.beetles)
                beetle.isLife = false;
            for (Gunshot gunshot : Gunshot.getShots()) {
                gunshot.locationX = 10000;
            }


            Tank.enemyTanks.set(0, new Tank(1100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 1700, "Resources\\Images\\BigEnemy2.png", "Resources\\Images\\BigEnemyGun2.png"));
            Tank.enemyTanks.set(1, new Tank(1100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 1400, "Resources\\Images\\BigEnemy2.png", "Resources\\Images\\BigEnemyGun2.png"));
            Tank.enemyTanks.set(2, new Tank(1700, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 1400, "Resources\\Images\\BigEnemy2.png", "Resources\\Images\\BigEnemyGun2.png"));
            Tank.enemyTanks.set(3, new FixedEnemy(14 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 3, "Resources\\Images\\FixedEnemy3.png", "Resources\\Images\\FixedEnemy4.png"));
            Tank.enemyTanks.set(4, new FixedEnemy(17 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 23, "Resources\\Images\\FixedEnemy3.png", "Resources\\Images\\FixedEnemy4.png"));
            Tank.enemyTanks.set(5, new FixedEnemy(11 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 15, "Resources\\Images\\FixedEnemy3.png", "Resources\\Images\\FixedEnemy4.png"));
            Tank.enemyTanks.set(6, new FixedEnemy(5 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 12, "Resources\\Images\\FixedEnemy3.png", "Resources\\Images\\FixedEnemy4.png"));
            Tank.enemyTanks.set(7, new FixedEnemy(2 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 23, "Resources\\Images\\FixedEnemy3.png", "Resources\\Images\\FixedEnemy4.png"));
            Tank.enemyTanks.set(8, new RocketFixedEnemy(16 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 2, "Resources\\Images\\FixedEnemy.png", "Resources\\Images\\FixedEnemy2.png"));


            ShotBox.ShotBoxes.get(0).locX = 400;
            ShotBox.ShotBoxes.get(0).locY = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100;
            ShotBox.ShotBoxes.get(0).isVisible = true;
            ShotBox.ShotBoxes.get(1).locX = 100;
            ShotBox.ShotBoxes.get(1).locY = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 200;
            ShotBox.ShotBoxes.get(1).isVisible = true;
            ShotBox.ShotBoxes.get(2).locX = 100;
            ShotBox.ShotBoxes.get(2).locY = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 300;
            ShotBox.ShotBoxes.get(2).isVisible = true;
            ShotBox.ShotBoxes.get(3).locX = 1710;
            ShotBox.ShotBoxes.get(3).locY = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 2300 + 10;
            ShotBox.ShotBoxes.get(3).isVisible = true;
            ShotBox.ShotBoxes.get(4).locX = 100;
            ShotBox.ShotBoxes.get(4).locY = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100;
            ShotBox.ShotBoxes.get(4).isVisible = true;
            ShotBox.ShotBoxes.get(5).locX = 400;
            ShotBox.ShotBoxes.get(5).locY = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 500;
            ShotBox.ShotBoxes.get(5).isVisible = true;

//            ShotBox.ShotBoxes.add(new ShotBox(100, 1000));
//            ShotBox.ShotBoxes.add(new Repairbox(100, 900));
//            ShotBox.ShotBoxes.add(new MachineGunShotBox(100, 850));
//            ShotBox.ShotBoxes.add(new WeaponUpdater(16 * 100+10, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 2 +10));
//            ShotBox.ShotBoxes.add(new LevelUpdater(17*100 , GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 22));


            GameMap.readMap("map2.txt");
            isVisible = false;
        }

    }

}

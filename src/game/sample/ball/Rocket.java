package game.sample.ball;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Rocket extends Beetle {

    /**
     * Field
     */
    public int numberOfMove = 0;


    /**
     * Constructor
     *
     * @param x       is x location of rocket
     * @param y       is y location of rocket
     * @param imgPath is the path of rocket image
     */
    public Rocket(int x, int y, String imgPath) {
        super(x, y, imgPath);
    }

    @Override
    public void move(int x, int y) {
        if (numberOfMove % 5 == 0) {
            try {
                image = ImageIO.read(new File("Resources\\Images\\enemyRocket.png"));
            } catch (IOException e) {

            }
        } else if (numberOfMove % 5 == 3) {
            try {
                image = ImageIO.read(new File("Resources\\Images\\enemyRocket.png"));
            } catch (IOException e) {

            }
        }


        lifeOfBeetle -= collisionShots();
        colision(x, y);

        if (lifeOfBeetle <= 0) {
            new SoundPlayer(false, "Resources\\Sounds\\enemydestroyed.wav").execute();
            isLife = false;
        }

        if (x + 86 > locX + 50)
            angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(0);

        else if (x + 86 < locX + 50) {
            angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(180);
        }

        if (true) {
            isVisible = true;
            locY -= -10 * Math.sin(angle);
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4))
                        flag = true;
                }
            }
            locY += -10 * Math.sin(angle);
            if (Math.abs(y - (locY - 10 * Math.sin(angle))) < Math.abs(y - locY)) {
                if (!flag)
                    locY -= 10 * Math.sin(angle);
            }


            locX -= -10 * Math.cos(angle);
            boolean flag2 = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4)) {
                        flag2 = true;
                    }
                }
            }
            locX += -10 * Math.cos(angle);

            if (!flag2)
                locX -= -10 * Math.cos(angle);
        }
        numberOfMove++;
    }

    /**
     * This method is for check collision of player's tank with object blocks
     *
     * @param objectBlock = an object block
     * @return true or false
     */
    @Override

    public boolean colision(ObjectBlock objectBlock) {
        Rectangle r = new Rectangle(objectBlock.x, objectBlock.y, objectBlock.image.getWidth(), objectBlock.image.getHeight());
        Rectangle p = new Rectangle(locX + 14, locY + 14, 60, 60);

        // Assuming there is an intersect method, otherwise just handcompare the values
        if (p.intersects(r)) {
            if (objectBlock.type == 3 || objectBlock.type == 2 || objectBlock.type == 4) {
                this.isLife = false;
                if (objectBlock.type == 3)
                    objectBlock.softWallLevel -= 5;
            }
            return true;

        } else {
            return false;
        }
    }

}

package game.sample.ball;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class SaveData {

    /**
     * Fields
     */
    static String locX, locY, lifeOfTank, shot, rShot;


    /**
     * This method is for save the map in file
     */
    public void saveMap() {

        BufferedWriter bw = null;
        FileWriter fw = null;
        BufferedWriter bw2 = null;
        FileWriter fw2 = null;

        try {

            fw = new FileWriter("saveMap.txt");
            bw = new BufferedWriter(fw);
            for (int i = 0; i < 25; i++) {
                for (int j = 0; j < 20; j++) {
                    if (GameMap.map[j][i].type == 1)
                        bw.write("s" + "\n");
                    if (GameMap.map[j][i].type == 2)
                        bw.write("hw" + "\n");
                    if (GameMap.map[j][i].type == 3)
                        bw.write("sw" + "\n");
                    if (GameMap.map[j][i].type == 4)
                        bw.write("t" + "\n");
                    if (GameMap.map[j][i].type == 5)
                        bw.write("p" + "\n");

                }
            }
            fw2 = new FileWriter("levelNumber.txt");
            bw2 = new BufferedWriter(fw);
            bw.write(GameFrame.numberOfMap + "\n");

            bw.write(GameMap.map[0][0].y + "\n");
            bw.write(GameMap.map[0][0].x + "\n");

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }


    }

    /**
     * This method is for save shots in the file
     */
    public void saveShots() {

        try (FileOutputStream listOfDownload = new FileOutputStream("saveShots.txt")) {

            ObjectOutputStream listOfDownloads = new ObjectOutputStream(listOfDownload);

            for (Gunshot gunshot : Gunshot.getShots()) {

                listOfDownloads.writeObject(gunshot);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is for save the location of player tank, life of tank and limits of shots in file
     */
    public void savePlayerTank() {
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            fw = new FileWriter("savePlayer.txt");
            bw = new BufferedWriter(fw);

            bw.write(GameState.locX + "\n");
            bw.write(GameState.locY + "\n");
            bw.write(GameState.lifeOfTank + "\n");
            bw.write(GameState.limitOfShots + "\n");
            bw.write(GameState.limitOfMachineGun + "\n");


        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
    }

    /**
     * This method is for save location of boxes and are live or no in file
     */
    public void saveBox() {

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            fw = new FileWriter("saveBox.txt");
            bw = new BufferedWriter(fw);
            for (int i = 0; i < 6; i++) {
                bw.write(ShotBox.ShotBoxes.get(i).locX + "\n");
                bw.write(ShotBox.ShotBoxes.get(i).locY + "\n");
                if (ShotBox.ShotBoxes.get(i).isVisible)
                    bw.write(1 + "\n");
                else
                    bw.write(2 + "\n");
            }
        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }


    }

    /**
     * This method is for save the location of enemy tanks and life in file
     */
    public void enemySave() {

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            fw = new FileWriter("saveEnemy.txt");
            bw = new BufferedWriter(fw);
            for (int i = 0; i < 9; i++) {
                bw.write(Tank.enemyTanks.get(i).locX + "\n");
                bw.write(Tank.enemyTanks.get(i).locY + "\n");
                if (Tank.enemyTanks.get(i).isLife)
                    bw.write(1 + "\n");
                else
                    bw.write(2 + "\n");
            }
        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }


    }

    /**
     * This method is for save location of beetle and are live or no in file
     */
    public void beetleSave() {

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            fw = new FileWriter("saveBeetle.txt");
            bw = new BufferedWriter(fw);
            for (int i = 0; i < 3; i++) {
                bw.write(Beetle.beetles.get(i).locX + "\n");
                bw.write(Beetle.beetles.get(i).locY + "\n");
                if (Beetle.beetles.get(i).isLife)
                    bw.write(1 + "\n");
                else
                    bw.write(2 + "\n");
            }
        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }


    }

    /**
     * This method is for read the map from file
     */
    public void readMap() {
        try (BufferedReader br = new BufferedReader(new FileReader("saveMap.txt"))) {

            String sCurrentLine;
            int n = 0;
            while ((sCurrentLine = br.readLine()) != null && n < 499) {

                if (sCurrentLine.equals("s")) {
                    GameMap.mapLabel[n % 20][n / 20] = new JTextField("s");
                    ObjectBlock object = new ObjectBlock(1, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\Area.png");
                    GameMap.map[n % 20][n / 20] = object;
                } else if (sCurrentLine.equals("hw")) {
                    GameMap.mapLabel[n % 20][n / 20] = new JTextField("hw");
                    ObjectBlock object = new ObjectBlock(2, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\hardWall.png");
                    GameMap.map[n % 20][n / 20] = object;
                } else if (sCurrentLine.equals("sw")) {
                    GameMap.mapLabel[n % 20][n / 20] = new JTextField("sw");
                    ObjectBlock object = new ObjectBlock(3, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\softWall.png");
                    GameMap.map[n % 20][n / 20] = object;
                } else if (sCurrentLine.equals("t")) {
                    GameMap.mapLabel[n % 20][n / 20] = new JTextField("t");
                    ObjectBlock object = new ObjectBlock(4, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\teazel2.png");
                    GameMap.map[n % 20][n / 20] = object;
                } else if (sCurrentLine.equals("p")) {
                    GameMap.mapLabel[n % 20][n / 20] = new JTextField("p");
                    ObjectBlock object = new ObjectBlock(5, (n % 20) * 100, (-1) * (n / 20) * 100 + GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight(), "Resources\\Images\\plant.png");
                    GameMap.map[n % 20][n / 20] = object;
                }
                n++;

            }
            String numOfMap = br.readLine();
            if (numOfMap.equals("1"))
                GameFrame.numberOfMap = 1;
            else
                GameFrame.numberOfMap = 2;
            String locX = br.readLine();
            String locY = br.readLine();
            System.out.println(locX + locY);
            Integer y;
            y = Integer.parseInt(locX);
            System.out.println(y);

            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    GameMap.map[i][j].y += (y - 1080);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is for read the shots from file
     */
    public void readShots() {
        try (FileInputStream filterDownload = new FileInputStream("saveShots.txt")) {

            ObjectInputStream filterDownloads = new ObjectInputStream(filterDownload);

            Gunshot gunshot = (Gunshot) filterDownloads.readObject();
            while (gunshot != null) {
                Gunshot.getShots().add(gunshot);
                gunshot = (Gunshot) filterDownloads.readObject();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * This method is for read the location of pkayer tank, life and shots from file
     */
    public void readPlayerTank() {
        try (BufferedReader br = new BufferedReader(new FileReader("savePlayer.txt"))) {


            locX = br.readLine();
            locY = br.readLine();
            lifeOfTank = br.readLine();
            shot = br.readLine();
            rShot = br.readLine();
            System.out.println(Integer.parseInt(locX));
            GameState.locX = Integer.parseInt(locX);
            GameState.locY = Integer.parseInt(locY);
            GameState.lifeOfTank = Integer.parseInt(lifeOfTank);
            GameState.limitOfShots = Integer.parseInt(shot);
            GameState.limitOfMachineGun = Integer.parseInt(rShot);


        } catch (IOException e) {
        }
    }

    /**
     * This method is for read the location of enemy tanks and are live or no from the file
     */
    public void readEnemy() {
        try (BufferedReader br = new BufferedReader(new FileReader("saveEnemy.txt"))) {
            if (GameFrame.numberOfMap == 1) {
                for (int i = 0; i < 9; i++) {
                    locX = br.readLine();
                    locY = br.readLine();
                    boolean isLife = false;
                    String str = br.readLine();
                    if (str.equals("1"))
                        isLife = true;
                    else
                        isLife = false;
                    Tank.enemyTanks.get(i).locX = Integer.parseInt(locX);
                    Tank.enemyTanks.get(i).locY = Integer.parseInt(locY);
                    Tank.enemyTanks.get(i).isLife = isLife;

                }
            } else {
                for (int i = 9; i < 18; i++) {
                    locX = br.readLine();
                    locY = br.readLine();
                    boolean isLife = false;
                    String str = br.readLine();
                    if (str.equals("1"))
                        isLife = true;
                    else
                        isLife = false;
                    Tank.enemyTanks.get(i).locX = Integer.parseInt(locX);
                    Tank.enemyTanks.get(i).locY = Integer.parseInt(locY);
                    Tank.enemyTanks.get(i).isLife = isLife;

                }
            }

        } catch (IOException e) {
        }
    }

    /**
     * This method is for read the location of beetles and are live or no from the file
     */
    public void readBeetle() {
        try (BufferedReader br = new BufferedReader(new FileReader("saveBeetle.txt"))) {

            for (int i = 0; i < 3; i++) {
                locX = br.readLine();
                locY = br.readLine();
                boolean isLife = false;
                String str = br.readLine();
                if (str.equals("1"))
                    isLife = true;
                else
                    isLife = false;
                Beetle.beetles.get(i).locX = Integer.parseInt(locX);
                Beetle.beetles.get(i).locY = Integer.parseInt(locY);
                Beetle.beetles.get(i).isLife = isLife;

            }


        } catch (IOException e) {
        }
    }

    /**
     * This method is for read the location of bexes and is visiable or no from the file
     */
    public void readBox() {
        try (BufferedReader br = new BufferedReader(new FileReader("saveBox.txt"))) {

            for (int i = 0; i < 6; i++) {
                locX = br.readLine();
                locY = br.readLine();
                boolean isLife = false;
                String str = br.readLine();
                if (str.equals("1"))
                    isLife = true;
                else
                    isLife = false;
                ShotBox.ShotBoxes.get(i).locX = Integer.parseInt(locX);
                ShotBox.ShotBoxes.get(i).locY = Integer.parseInt(locY);
                ShotBox.ShotBoxes.get(i).isVisible = isLife;

            }


        } catch (IOException e) {
        }
    }
}
/*** In The Name of Allah ***/
package game.sample.ball;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * The window on which the rendering is performed.
 * This example uses the modern BufferStrategy approach for double-buffering,
 * actually it performs triple-buffering!
 * For more information on BufferStrategy check out:
 * http://docs.oracle.com/javase/tutorial/extra/fullscreen/bufferstrategy.html
 * http://docs.oracle.com/javase/8/docs/api/java/awt/image/BufferStrategy.html
 *
 * @author Seyed Mohammad Ghaffarian
 */
public class GameFrame extends JFrame {
    public static int numberOfMap = 1;
    public static boolean isPaused;
    public static final int GAME_HEIGHT = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight();                  // 720p game resolution
    public static final int GAME_WIDTH = 16 * GAME_HEIGHT / 9;  // wide aspect ratio

    //uncomment all /*...*/ in the class for using Tank icon instead of a simple circle
    /*private BufferedImage image;*/

    private long lastRender;
    private ArrayList<Float> fpsHistory;
    private BufferStrategy bufferStrategy;
    //


    public GameFrame(String title) {

        super(title);
        setResizable(false);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
        lastRender = -1;
        fpsHistory = new ArrayList<>(100);
        /**
         * add enemy Tank
         */
        if (true) {
            Tank.enemyTanks.add(new Tank(1200, 800, "Resources\\Images\\BigEnemy.png", "Resources\\Images\\BigEnemyGun.png"));
            Tank.enemyTanks.add(new Tank(1200, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 1200, "Resources\\Images\\BigEnemy2.png", "Resources\\Images\\BigEnemyGun2.png"));
            Tank.enemyTanks.add(new Tank(600, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 2300, "Resources\\Images\\BigEnemy2.png", "Resources\\Images\\BigEnemyGun2.png"));
            Tank.enemyTanks.add(new Tank(400, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 1733300, "Resources\\Images\\BigEnemy2.png", "Resources\\Images\\BigEnemyGun2.png"));
            Tank.enemyTanks.add(new FixedEnemy(17 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 23, "Resources\\Images\\FixedEnemy3.png", "Resources\\Images\\FixedEnemy4.png"));
            Tank.enemyTanks.add(new FixedEnemy(11 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 9, "Resources\\Images\\FixedEnemy3.png", "Resources\\Images\\FixedEnemy4.png"));
            Tank.enemyTanks.add(new FixedEnemy(17 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 19, "Resources\\Images\\FixedEnemy3.png", "Resources\\Images\\FixedEnemy4.png"));
            Tank.enemyTanks.add(new FixedEnemy(9 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 16, "Resources\\Images\\FixedEnemy3.png", "Resources\\Images\\FixedEnemy4.png"));
            Tank.enemyTanks.add(new RocketFixedEnemy(16 * 100, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 2, "Resources\\Images\\FixedEnemy.png", "Resources\\Images\\FixedEnemy2.png"));

            Beetle.beetles.add(new Beetle(1200, 800, "Resources\\Images\\Enemy1.png"));
            Beetle.beetles.add(new Beetle(700, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 2300, "Resources\\Images\\Enemy1.png"));
            Beetle.beetles.add(new Beetle(1400, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 2300, "Resources\\Images\\Enemy1.png"));
            ShotBox.ShotBoxes.add(new ShotBox(100, 1000));
            ShotBox.ShotBoxes.add(new Repairbox(100, 900));
            ShotBox.ShotBoxes.add(new MachineGunShotBox(100, 850));
            ShotBox.ShotBoxes.add(new WeaponUpdater(16 * 100 + 10, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 2 + 10));
            ShotBox.ShotBoxes.add(new LevelUpdater(1700, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 22));
            GameWin gameWin = new GameWin(400, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 4);
            gameWin.isVisible = false;
            ShotBox.ShotBoxes.add(gameWin);
            ShotBox.ShotBoxes.add(new ShotBox(600, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight() - 100 * 21));

            if (Menu.isCountinue) {
                new SaveData().readEnemy();
                new SaveData().readBeetle();
                new SaveData().readBox();

            }

        }

    }

    /**
     * This must be called once after the JFrame is shown:
     * frame.setVisible(true);
     * and before any rendering is started.
     */
    public void initBufferStrategy() {
        // Triple-buffering
        createBufferStrategy(3);
        bufferStrategy = getBufferStrategy();
    }


    /**
     * Game rendering with triple-buffering using BufferStrategy.
     */
    public void render(GameState state) {
        // Render single frame
        do {
            // The following loop ensures that the contents of the drawing buffer
            // are consistent in case the underlying surface was recreated
            do {
                // Get a new graphics context every time through the loop
                // to make sure the strategy is validated
                Graphics2D graphics = (Graphics2D) bufferStrategy.getDrawGraphics();
                try {
                    doRendering(graphics, state);
                } finally {
                    // Dispose the graphics
                    graphics.dispose();
                }
                // Repeat the rendering if the drawing buffer contents were restored
            } while (bufferStrategy.contentsRestored());

            // Display the buffer
            bufferStrategy.show();
            // Tell the system to do the drawing NOW;
            // otherwise it can take a few extra ms and will feel jerky!
            Toolkit.getDefaultToolkit().sync();

            // Repeat the rendering if the drawing buffer was lost
        } while (bufferStrategy.contentsLost());
    }

    /**
     * Rendering all game elements based on the game state.
     */
    private void doRendering(Graphics2D g2d, GameState state) {

        // if (!isPaused) {
        //create buffered image for every image in game
        BufferedImage img = null;
        BufferedImage img2 = null;
        BufferedImage heart = null;
        BufferedImage limitOfShots = null;
        BufferedImage limitOfMachineGun = null;
        BufferedImage PauseImage = null;

        //change mouse cursor
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image image = toolkit.getImage("Resources\\Images\\cursor.png");
        Cursor c = toolkit.createCustomCursor(image, new Point(0, 0), "img");
        getContentPane().setCursor(c);

        // Draw background
        g2d.setColor(Color.GRAY);
        g2d.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);

        //Draw map
        for (int i = 0; i < 20; i++)
            for (int j = 0; j < 25; j++) {
                if (GameMap.map[i][j].type == 3 && GameMap.map[i][j].softWallLevel > 10 && GameMap.map[i][j].softWallLevel <= 15)
                    GameMap.map[i][j].setimg("Resources\\Images\\softWall1.png");
                else if (GameMap.map[i][j].type == 3 && GameMap.map[i][j].softWallLevel > 5 && GameMap.map[i][j].softWallLevel <= 10)
                    GameMap.map[i][j].setimg("Resources\\Images\\softWall2.png");
                else if (GameMap.map[i][j].type == 3 && GameMap.map[i][j].softWallLevel <= 5 && GameMap.map[i][j].softWallLevel > 0)
                    GameMap.map[i][j].setimg("Resources\\Images\\softWall3.png");
                else if (GameMap.map[i][j].type == 3 && GameMap.map[i][j].softWallLevel <= 0) {
                    GameMap.map[i][j].setimg("Resources\\Images\\Area.png");
                    GameMap.map[i][j].type = 1;
                }

                if (GameMap.map[i][j].type == 5) {
                    BufferedImage soilImg = null;
                    try {
                        soilImg = ImageIO.read(new File("Resources\\Images\\Area.png"));
                    } catch (IOException e) {
                    }
                    g2d.drawImage(soilImg, GameMap.map[i][j].x, GameMap.map[i][j].y, null);
                } else
                    g2d.drawImage(GameMap.map[i][j].image, GameMap.map[i][j].x, GameMap.map[i][j].y, null);

                if (GameMap.map[i][j].type == 4) {
                    BufferedImage soilImg = null;
                    try {
                        soilImg = ImageIO.read(new File("Resources\\Images\\Area.png"));
                    } catch (IOException e) {
                    }
                    g2d.drawImage(soilImg, GameMap.map[i][j].x, GameMap.map[i][j].y, null);
                    g2d.drawImage(GameMap.map[i][j].image, GameMap.map[i][j].x, GameMap.map[i][j].y, null);

                }

            }

        //Draw shots
        if (Gunshot.getShots().size() <= 30) {
            for (int i = 0; i < Gunshot.getShots().size(); i++) {
                //rotate shot image
                double rotationRequired = -Gunshot.getShots().get(i).getAngle() + Math.toRadians(90);
                double locationX = Gunshot.getShots().get(i).getImg().getWidth() / 2;
                if (true) {
                }
                double locationY = Gunshot.getShots().get(i).getImg().getHeight() / 2;
                AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
                AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
                // Drawing the rotated image at the required drawing locations
                g2d.drawImage(op.filter(Gunshot.getShots().get(i).getImg(), null), Gunshot.getShots().get(i).locationX, Gunshot.getShots().get(i).locationY, null);
            }
        } else {
            for (int i = Gunshot.getShots().size() - 30; i < Gunshot.getShots().size(); i++) {
                //rotate shot image
                double rotationRequired = -Gunshot.getShots().get(i).getAngle() + Math.toRadians(90);
                double locationX = Gunshot.getShots().get(i).getImg().getWidth() / 2;
                double locationY = Gunshot.getShots().get(i).getImg().getHeight() / 2;
                AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
                AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
                // Drawing the rotated image at the required drawing locations
                g2d.drawImage(op.filter(Gunshot.getShots().get(i).getImg(), null), Gunshot.getShots().get(i).locationX, Gunshot.getShots().get(i).locationY, null);
            }
        }

        // initialize buffered image
        g2d.setColor(Color.BLACK);
        try {
            img = state.tank;
            img2 = ImageIO.read(new File(state.gunShotPath));
            heart = ImageIO.read(new File("Resources\\Images\\heart.png"));
            limitOfShots = ImageIO.read(new File("Resources\\Images\\NumberOfHeavyBullet.png"));
            limitOfMachineGun = ImageIO.read(new File("Resources\\Images\\NumberOfMachinGun2.png"));
            PauseImage = ImageIO.read(new File("Resources\\Images\\Pause-icon.png"));
        } catch (IOException e) {
        }


        //rotate tank body
        {
            //rotate image
            double rotationRequired = state.angleOFBodyOfTank2;
            double locationX = img.getWidth() / 2;
            double locationY = img.getHeight() / 2;
            AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
            AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);

            // Drawing the rotated image at the required drawing locations
            g2d.drawImage(op.filter(img, null), state.locX - 184, state.locY - 172, null);
        }
        //rotate tank gun
        {
            //rotate image
            double rotationRequired = state.angle;
            double locationX = img2.getWidth() / 2 - 5;
            double locationY = img2.getHeight() / 2;
            AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
            AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);

            // Drawing the rotated image at the required drawing locations
            g2d.drawImage(op.filter(img2, null), state.locX, state.locY, null);
        }


        //showing ShotBox, repairBox, weaponUpdater and machineGunShotBox
        for (ShotBox shotBoxes : ShotBox.ShotBoxes) {
            if (shotBoxes.isVisible) {
                g2d.drawImage(shotBoxes.image, shotBoxes.locX, shotBoxes.locY, null);
                shotBoxes.colision(state.locX, state.locY);
            }
        }

        //drawing enemy tank
        for (Tank enemyTankObject : Tank.enemyTanks) {
            if (enemyTankObject.isLife) {

                enemyTankObject.move(state.locX, state.locY);
                //rotate enemy gun
                double rotationRequired = (-1) * enemyTankObject.angle;
                double locationX = enemyTankObject.gunImage.getWidth() / 2;
                double locationY = enemyTankObject.gunImage.getHeight() / 2;
                AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
                AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);

                // Drawing the rotated image at the required drawing locations
                g2d.drawImage(enemyTankObject.img, enemyTankObject.locX, enemyTankObject.locY, null);
                g2d.drawImage(op.filter(enemyTankObject.gunImage, null), enemyTankObject.locX, enemyTankObject.locY, null);
            }
        }

        //drawing beetles
        for (Beetle enemyTankObject : Beetle.beetles) {
            if (enemyTankObject.isLife)
                enemyTankObject.colision(state.locX, state.locY);
            if (enemyTankObject.isLife && enemyTankObject.isVisible) {
                enemyTankObject.move(state.locX, state.locY);

                //rotate image of beetle
                double rotationRequired = (-1) * enemyTankObject.angle;
                double locationX = enemyTankObject.image.getWidth() / 2;
                double locationY = enemyTankObject.image.getHeight() / 2;
                AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
                AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
                // Drawing the rotated image at the required drawing locations
                g2d.drawImage(op.filter(enemyTankObject.image, null), enemyTankObject.locX, enemyTankObject.locY, null);
            }
        }


        // Drawing plant
        for (int i = 0; i < 20; i++)
            for (int j = 0; j < 25; j++) {
                if (GameMap.map[i][j].type == 5)
                    g2d.drawImage(GameMap.map[i][j].image, GameMap.map[i][j].x, GameMap.map[i][j].y, null);

            }

        //drawing trazel
        for (int i = 0; i < 20; i++)
            for (int j = 0; j < 25; j++) {
                if (GameMap.map[i][j].type == 4)
                    g2d.drawImage(GameMap.map[i][j].image, GameMap.map[i][j].x, GameMap.map[i][j].y, null);

            }


        // Print user guide
        g2d.setColor(Color.CYAN);
        String userGuide = state.limitOfShots + "";
        for (int i = 0; i < GameState.lifeOfTank / 5; i++) {
            g2d.drawImage(heart, i * 50, GAME_HEIGHT - 70, null);
        }
        g2d.setFont(g2d.getFont().deriveFont(30.0f));
        g2d.drawImage(limitOfShots, 10, 50, null);
        g2d.drawString(userGuide, 35, 100);

        String userGuide2 = state.limitOfMachineGun + "";
        g2d.setFont(g2d.getFont().deriveFont(20.0f));
        g2d.setFont(g2d.getFont().deriveFont(20));
        //Draw limit of machine gun
        g2d.drawImage(limitOfMachineGun, 10, 100, null);
        g2d.drawString(userGuide2, 35, 150);

        g2d.drawImage(PauseImage, 10, 150, null);

        if (state.gameOver) {
            new GameOver().show();
        }
    }

}

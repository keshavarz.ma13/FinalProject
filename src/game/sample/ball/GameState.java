/*** In The Name of Allah ***/
package game.sample.ball;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


/**
 * This class holds the state of game and all of its elements.
 * This class also handles user inputs, which affect the game state.
 *
 * @author Seyed Mohammad Ghaffarian
 */
public class GameState {

    public static int locX, locY, diam;
    public double angle, angleOFBodyOfTank, angleOFBodyOfTank2;
    public boolean gameOver;
    public BufferedImage tank;
    String gunShotImage = "";
    private boolean keyUP, keyDOWN, keyRIGHT, keyLEFT;
    private boolean key1, key2, key3;
    private boolean mousePress;
    public double mouseX, mouseY;
    private KeyHandler keyHandler;
    private MouseHandler mouseHandler;
    private int numberOfUpdate;
    public static int lifeOfTank;
    public static int limitOfShots;
    public static int limitOfMachineGun;
    String gunShotPath = "Resources\\Images\\tankGun01.png";
    static boolean isMachineGun = false;
    static int weaponPower = 1;


    public GameState() {
        locX = 100;
        locY = 100;

        diam = 200;
        gameOver = false;
        //
        keyUP = false;
        keyDOWN = false;
        keyRIGHT = false;
        keyLEFT = false;
        //
        angle = 0;
        //
        mousePress = false;
        mouseX = 0;
        mouseY = 0;
        //
        keyHandler = new KeyHandler();
        mouseHandler = new MouseHandler();
        //
        lifeOfTank = 50 / Menu.Level;
        limitOfShots = 45;
        limitOfMachineGun = 400;
        //
        try {
            tank = ImageIO.read(new File("Resources\\Images\\tank 1.png"));
        } catch (IOException e) {

        }

        if (Menu.isCountinue) {
            new SaveData().readPlayerTank();
            new SaveData().readShots();
            GameState.locX = Integer.parseInt(SaveData.locX);
            GameState.locY = Integer.parseInt(SaveData.locY);
            GameState.lifeOfTank = Integer.parseInt(SaveData.lifeOfTank);
            GameState.limitOfShots = Integer.parseInt(SaveData.shot);
            GameState.limitOfMachineGun = Integer.parseInt(SaveData.rShot);
        }
    }

    /**
     * The method which updates the game state.
     */
    public void update() {
        if (keyDOWN || keyUP || keyLEFT || keyRIGHT) {
            if (numberOfUpdate % 2 == 1) {
                try {
                    tank = ImageIO.read(new File("Resources\\Images\\tank 1.png"));
                } catch (IOException e) {

                }
            }
            if (numberOfUpdate % 2 == 0) {
                try {
                    tank = ImageIO.read(new File("Resources\\Images\\tank 2.png"));
                } catch (IOException e) {

                }
            }
        }


        if (angleOFBodyOfTank2 < angleOFBodyOfTank && Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(8)) {

            angleOFBodyOfTank2 += Math.toRadians(15);

        } else if (angleOFBodyOfTank < angleOFBodyOfTank2 && Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(8)) {

            angleOFBodyOfTank2 -= Math.toRadians(15);
        }

        lifeOfTank -= collisionShots();
        if (lifeOfTank <= 0) {
            gameOver = true;
        }
        Gunshot.update();

        if (mouseX > locX + 86)
            angle = Math.atan((mouseY - (locY + 92.00)) / (mouseX - (locX + 86)));
        else if (mouseX < locX + 86) {
            angle = Math.atan((mouseY - (locY + 92)) / (mouseX - (locX + 86))) + Math.toRadians(180);
        }


        if (keyUP) {


            locY -= 8;
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4))
                        flag = true;
                }
            }
            locY += 8;

            if (!flag) {
                if (locY > 200) {
                    GameMap.updateMap(1, 0);
                    for (Tank enemyTankObject : Tank.enemyTanks)
                        enemyTankObject.changLocation(0, +4);
                    for (Beetle beetle : Beetle.beetles)
                        beetle.changLocation(0, +4);
                    for (ShotBox beetle : ShotBox.ShotBoxes)
                        beetle.locY += 4;
                    for (Gunshot gunshot : Gunshot.getShots())
                        gunshot.locationY += 4;
                    locY -= 4;
                } else {
                    GameMap.updateMap(2, 0);
                    for (Tank enemyTankObject : Tank.enemyTanks)
                        enemyTankObject.changLocation(0, +8);
                    for (Beetle beetle : Beetle.beetles)
                        beetle.changLocation(0, +8);
                    for (ShotBox beetle : ShotBox.ShotBoxes)
                        beetle.locY += 8;
                    for (Gunshot gunshot : Gunshot.getShots())
                        gunshot.locationY += 8;
                }
            }
        }
        if (keyDOWN) {
            locY += 8;
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4))
                        flag = true;
                }
            }
            locY -= 8;
            if (!flag) {
                if (locY < GameFrame.GAME_HEIGHT - diam - 200) {
                    GameMap.updateMap(0, 1);
                    for (Tank enemyTankObject : Tank.enemyTanks)
                        enemyTankObject.changLocation(0, -4);
                    for (Beetle beetle : Beetle.beetles)
                        beetle.changLocation(0, -4);
                    for (ShotBox beetle : ShotBox.ShotBoxes)
                        beetle.locY -= 4;
                    for (Gunshot gunshot : Gunshot.getShots())
                        gunshot.locationY -= 4;
                    locY += 4;
                } else {
                    GameMap.updateMap(0, 2);
                    for (Tank enemyTankObject : Tank.enemyTanks)
                        enemyTankObject.changLocation(0, -8);
                    for (Beetle beetle : Beetle.beetles)
                        beetle.changLocation(0, -8);
                    for (ShotBox beetle : ShotBox.ShotBoxes)
                        beetle.locY -= 8;
                    for (Gunshot gunshot : Gunshot.getShots())
                        gunshot.locationY -= 8;
                }
            }
        }
        if (key1) {
            limitOfMachineGun++;
        }
        if (key2) {
            limitOfShots++;
        }
        if (key3) {
            lifeOfTank = 50 / Menu.Level;
        }
        if (keyLEFT) {
            locX -= 8;
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4))
                        flag = true;
                }
            }
            locX += 8;

            if (!flag) {
                locX -= 8;
            }
        }
        if (keyRIGHT) {
            locX += 8;
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4))
                        flag = true;
                }
            }
            locX -= 8;
            if (!flag) {
                locX += 8;
            }
        }

        if (keyUP && keyRIGHT) {
            angleOFBodyOfTank = -Math.toRadians(45);
            if (Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(180)) {
                angleOFBodyOfTank = Math.toRadians(360 - 45);
            }
        }
        if (keyUP && keyLEFT) {
            angleOFBodyOfTank = -Math.toRadians(45 + 90);
            if (Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(180)) {
                angleOFBodyOfTank = Math.toRadians(360 - 45 - 90);
            }
        }
        if (keyDOWN && keyRIGHT) {
            angleOFBodyOfTank = Math.toRadians(45);
            if (Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(180)) {
                angleOFBodyOfTank = Math.toRadians(-360 + 45);
            }
        }
        if (keyDOWN && keyLEFT) {
            angleOFBodyOfTank = Math.toRadians(45 + 90);
            if (Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(180)) {
                angleOFBodyOfTank = Math.toRadians(-360 + 45 + 90);
            }
        }
        if (keyUP && !keyRIGHT && !keyLEFT && !keyDOWN) {
            angleOFBodyOfTank = -Math.toRadians(90);
            if (Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(180)) {
                angleOFBodyOfTank = Math.toRadians(270);
            }
        }
        if (!keyUP && keyRIGHT && !keyLEFT && !keyDOWN) {
            angleOFBodyOfTank = -Math.toRadians(0);
            if (Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(180)) {
                angleOFBodyOfTank = Math.toRadians(360);
            }
        }
        if (!keyUP && !keyRIGHT && keyLEFT && !keyDOWN) {
            angleOFBodyOfTank = Math.toRadians(180);
            if (Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(180)) {
                angleOFBodyOfTank = -Math.toRadians(180);
            }

        }
        if (!keyUP && !keyRIGHT && !keyLEFT && keyDOWN) {
            angleOFBodyOfTank = Math.toRadians(90);
            if (Math.abs(angleOFBodyOfTank2 - angleOFBodyOfTank) > Math.toRadians(180)) {
                angleOFBodyOfTank = -Math.toRadians(270);
            }
        }


        locX = Math.max(locX, 0);
        locX = Math.min(locX, GameFrame.GAME_WIDTH - diam);
        locY = Math.max(locY, 0);
        locY = Math.min(locY, GameFrame.GAME_HEIGHT - diam);
        numberOfUpdate++;
    }


    public KeyListener getKeyListener() {
        return keyHandler;
    }

    public MouseListener getMouseListener() {
        return mouseHandler;
    }

    public MouseMotionListener getMouseMotionListener() {
        return mouseHandler;
    }

    public boolean colision(ObjectBlock objectBlock) {
        Rectangle r = new Rectangle(objectBlock.x, objectBlock.y, objectBlock.image.getWidth(), objectBlock.image.getHeight());
        Rectangle p = new Rectangle(locX, locY, 170, 180);

        // Assuming there is an intersect method, otherwise just handcompare the values
        if (p.intersects(r)) {

            return true;
        } else {
            return false;
        }
    }

    public Integer collisionShots() {
        int number = 0;
        if (Gunshot.getShots().size() <= 30) {
            for (int i = 0; i < Gunshot.getShots().size(); i++) {
                Rectangle r = new Rectangle(Gunshot.getShots().get(i).locationX, Gunshot.getShots().get(i).locationY, Gunshot.getShots().get(i).getImg().getWidth(), Gunshot.getShots().get(i).getImg().getHeight());
                Rectangle p = new Rectangle(locX, locY, 170, 180);

                // Assuming there is an intersect method, otherwise just handcompare the values
                if (p.intersects(r) && Gunshot.getShots().get(i).fromEnemy) {
                    number++;
                    Gunshot.getShots().get(i).locationX = 10000;
                    Gunshot.getShots().get(i).locationY = 10000;
                }

            }
        } else {
            for (int i = Gunshot.getShots().size() - 30; i < Gunshot.getShots().size(); i++) {
                Rectangle r = new Rectangle(Gunshot.getShots().get(i).locationX, Gunshot.getShots().get(i).locationY, Gunshot.getShots().get(i).getImg().getWidth(), Gunshot.getShots().get(i).getImg().getHeight());
                Rectangle p = new Rectangle(locX, locY, 170, 180);

                // Assuming there is an intersect method, otherwise just handcompare the values
                if (p.intersects(r) && Gunshot.getShots().get(i).fromEnemy) {
                    number++;
                    Gunshot.getShots().get(i).locationX = 10000;
                    Gunshot.getShots().get(i).locationY = 10000;
                }
            }
        }
        return number;
    }

    /**
     * The keyboard handler.
     */
    class KeyHandler extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_UP:
                    keyUP = true;
                    break;
                case KeyEvent.VK_DOWN:
                    keyDOWN = true;
                    break;
                case KeyEvent.VK_LEFT:
                    keyLEFT = true;
                    break;
                case KeyEvent.VK_RIGHT:
                    keyRIGHT = true;
                    break;
                case KeyEvent.VK_W:
                    keyUP = true;
                    break;
                case KeyEvent.VK_A:
                    keyLEFT = true;
                    break;
                case KeyEvent.VK_D:
                    keyRIGHT = true;
                    break;
                case KeyEvent.VK_S:
                    keyDOWN = true;
                    break;
                case KeyEvent.VK_1:
                    key1 = true;
                    break;
                case KeyEvent.VK_2:
                    key2 = true;
                    break;
                case KeyEvent.VK_3:
                    key3 = true;
                    break;

            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_UP:
                    keyUP = false;
                    break;
                case KeyEvent.VK_DOWN:
                    keyDOWN = false;
                    break;
                case KeyEvent.VK_LEFT:
                    keyLEFT = false;
                    break;
                case KeyEvent.VK_RIGHT:
                    keyRIGHT = false;
                    break;
                case KeyEvent.VK_W:
                    keyUP = false;
                    break;
                case KeyEvent.VK_A:
                    keyLEFT = false;
                    break;
                case KeyEvent.VK_D:
                    keyRIGHT = false;
                    break;
                case KeyEvent.VK_S:
                    keyDOWN = false;
                    break;
                case KeyEvent.VK_1:
                    key1 = false;
                    break;
                case KeyEvent.VK_2:
                    key2 = false;
                    break;
                case KeyEvent.VK_3:
                    key3 = false;
                    break;

            }
        }

    }

    /**
     * The mouse handler.
     */
    class MouseHandler extends MouseAdapter {


        @Override
        public void mouseReleased(MouseEvent e) {
            if (limitOfShots <= 0) {
                gunShotPath = "Resources\\Images\\tankGun02.png";
                isMachineGun = true;
            }
            boolean flag = false;
            for (int i = 10; i < 10 + 50; i++)
                for (int j = 150; j < 200; j++) {
                    if (mouseX == i && mouseY == j)
                        flag = true;
                }
            if (flag) {
                GameFrame.isPaused = true;
                new PauseGame().show();
            }
            if (!flag && !GameFrame.isPaused) {
                if (e.getButton() == 1) {
                    if (limitOfShots > 0 || limitOfMachineGun > 0) {
                        if (!isMachineGun && limitOfShots > 0)
                            limitOfShots--;
                        else if (isMachineGun && limitOfMachineGun > 0)
                            limitOfMachineGun--;
                        if (!isMachineGun && limitOfShots > 0) {
                            Gunshot newShot = new Gunshot(locX + 100, locY + 100, angle, false, isMachineGun);
                            Gunshot.getShots().add(newShot);
                            new SoundPlayer(false, "Resources\\Sounds\\cannon.wav").execute();
                        } else if (isMachineGun && limitOfMachineGun > 0) {
                            Gunshot newShot = new Gunshot(locX + 100, locY + 100, angle, false, isMachineGun);
                            Gunshot.getShots().add(newShot);
                            new SoundPlayer(false, "Resources\\Sounds\\mashingun.wav").execute();
                            new SoundPlayer(false, "Resources\\Sounds\\recosh9.wav").execute();
                        }

                        if (!isMachineGun && limitOfShots <= 0)
                            new SoundPlayer(false, "Resources\\Sounds\\emptyGun.wav").execute();
                        else if (isMachineGun && limitOfMachineGun <= 0)
                            new SoundPlayer(false, "Resources\\Sounds\\emptyGun.wav").execute();

                        mousePress = false;
                    }


                } else if (e.getButton() == 3) {
                    if (isMachineGun == false) {
                        gunShotPath = "Resources\\Images\\tankGun02.png";
                        isMachineGun = true;
                    } else {
                        gunShotPath = "Resources\\Images\\tankGun01.png";
                        isMachineGun = false;
                    }

                }
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            mouseX = e.getX();
            mouseY = e.getY();
        }
    }


}

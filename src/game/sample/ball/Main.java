/*** In The Name of Allah ***/
package game.sample.ball;

import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 * Program start.
 *
 * @author Seyed Mohammad Ghaffarian
 */
public class Main {

    public static void main(String[] args) {
        // Initialize the global thread-pool
        ThreadPool.init();

        // Show the game menu ...

        // After the player clicks 'PLAY' ...
        EventQueue.invokeLater(new Runnable() {
            //run the game
            @Override
            public void run() {
                if (GameFrame.numberOfMap == 1)
                    GameMap.readMap("map.txt");
                else
                    GameMap.readMap("map2.txt");
                Menu menu = new Menu();
                menu.Show();


            }
        });
    }
}

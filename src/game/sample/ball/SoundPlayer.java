package game.sample.ball;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.PublicKey;

public class SoundPlayer extends SwingWorker<Boolean, Boolean> {

    /**
     * fields
     */
    boolean isBackGroundSound;
    String path;

    /**
     * constructor
     *
     * @param isBackGroundSound is for sound of background
     * @param path              is path of sound file
     */
    public SoundPlayer(boolean isBackGroundSound, String path) {
        this.isBackGroundSound = isBackGroundSound;
        this.path = path;
    }

    @Override
    protected Boolean doInBackground() throws Exception {
        // open the sound file as a Java input stream
        AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File(path));
        Clip clip = AudioSystem.getClip();
        clip.open(inputStream);
        if (isBackGroundSound)
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        else
            clip.loop(0);


        return null;
    }

    @Override
    protected void done() {
        if (isBackGroundSound)
            this.execute();
    }
}

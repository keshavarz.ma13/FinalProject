package game.sample.ball;

public class FixedEnemy extends Tank {
    /**
     * constructor
     * @param x = x location of fixed enemy
     * @param y = y location of fixed enemy
     * @param imgPath = path of the body of fixed enemy
     * @param imgPath2 = path of the gun of fixed enemy
     */
    public FixedEnemy( int x, int y, String imgPath, String imgPath2){
        super(x, y, imgPath, imgPath2);
        lifeOfEnemyTank = 25 ;
        angle = -Math.toRadians(90);
    }

    int number ;

    /**
     * find angel and shutting fixed enemy
     * @param x = x location of player tank
     * @param y = y location of player tank
     */
    @Override
    public void move(int x, int y) {
        lifeOfEnemyTank -= collisionShots();
        if (lifeOfEnemyTank <= 0) {
            new SoundPlayer(false, "Resources\\Sounds\\enemydestroyed.wav").execute();
            isLife = false;
        }
        if(number%10 == 0 ) {
            if(x<locX && y>locY) {
                if (Math.abs(locX - x) < 1000 && Math.abs(locY - y) < 1000 && locY > 0) {
                    new SoundPlayer(false, "Resources\\Sounds\\enemyshot.wav").execute();
                    Gunshot shot = new Gunshot(locX + 50, locY + 50, angle * (-1), true, false);
                    Gunshot.getShots().add(shot);
                }
            }

        }

        if(x<locX && y>locY) {
            if (x + 86 > locX + 50)
                angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(0);

            else if (x + 86 < locX + 50) {
                angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(180);
            }
        }

        number++;
    }

}

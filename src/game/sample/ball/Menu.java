package game.sample.ball;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowEvent;

public class Menu {

    /**
     * Fields
     */
    static int Level = 0;
    static boolean isCountinue;
    JFrame menuFrame = new JFrame("menu");
    JButton easy = new JButton("Easy");
    JButton normal = new JButton("Normal");
    JButton hard = new JButton("Hard");
    JButton editMap = new JButton("Edit Map");
    JButton continueButton = new JButton("Continue");
    static SoundPlayer soundPlayer = new SoundPlayer(false, "Resources\\Sounds\\startup.wav");

    /**
     * Constructor
     */
    public Menu() {
        actionhandler actionhandler = new actionhandler();
        menuFrame.setLayout(null);
        menuFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        menuFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        menuFrame.setUndecorated(true);
        ImageIcon backImage = new ImageIcon("Resources\\Images\\Startup.png");
        JLabel background = new JLabel(backImage);
        background.setBounds(new Rectangle(0, 0, GameFrame.GAME_WIDTH, GameFrame.GAME_HEIGHT));
        easy.setBounds(new Rectangle(200, 500, 100, 70));
        continueButton.setBounds(new Rectangle(200, 400, 100, 70));
        normal.setBounds(new Rectangle(200, 600, 100, 70));
        hard.setBounds(new Rectangle(200, 700, 100, 70));
        editMap.setBounds(new Rectangle(200, 800, 100, 70));

        menuFrame.add(continueButton);
        menuFrame.add(easy);
        menuFrame.add(normal);
        menuFrame.add(hard);
        menuFrame.add(editMap);
        continueButton.addActionListener(actionhandler);
        easy.addActionListener(actionhandler);
        normal.addActionListener(actionhandler);
        hard.addActionListener(actionhandler);
        editMap.addActionListener(actionhandler);
        menuFrame.add(background);
    }

    /**
     * Show the menu frame
     */
    public void Show() {
//       soundPlayer.execute();
        menuFrame.setVisible(true);
    }

    class actionhandler implements ActionListener {

        /**
         * Action listener
         *
         * @param e
         */
        @Override
        public void actionPerformed(ActionEvent e) {
//            Menu.soundPlayer;
//            Menu.soundPlayer.cancel(true);
            if (e.getSource().equals(easy)) {
                Level = 1;

                new SoundPlayer(true, "Resources\\Sounds\\gameSound1.wav").execute();
                menuFrame.setVisible(false);
                if (GameFrame.numberOfMap == 1)
                    GameMap.readMap("map.txt");
                else
                    GameMap.readMap("map2.txt");
                GameFrame frame = new GameFrame("Simple Ball !");
                frame.setLocationRelativeTo(null); // put frame at center of screen
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
                frame.initBufferStrategy();
                // Create and execute the game-loop
                GameLoop game = new GameLoop(frame);
                game.init();
                ThreadPool.execute(game);
                // and the game starts ...
            } else if (e.getSource().equals(normal)) {
                Level = 2;
                new SoundPlayer(true, "Resources\\Sounds\\gameSound1.wav").execute();
                if (GameFrame.numberOfMap == 1)
                    GameMap.readMap("map.txt");
                else
                    GameMap.readMap("map2.txt");

                menuFrame.setVisible(false);

                GameFrame frame = new GameFrame("Simple Ball !");
                frame.setLocationRelativeTo(null); // put frame at center of screen
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
                frame.initBufferStrategy();
                // Create and execute the game-loop
                GameLoop game = new GameLoop(frame);
                game.init();
                ThreadPool.execute(game);
                // and the game starts ...
            } else if (e.getSource().equals(hard)) {
                Level = 3;
                new SoundPlayer(true, "Resources\\Sounds\\gameSound1.wav").execute();
                menuFrame.setVisible(false);
                if (GameFrame.numberOfMap == 1)
                    GameMap.readMap("map.txt");
                else
                    GameMap.readMap("map2.txt");
                GameFrame frame = new GameFrame("Simple Ball !");
                frame.setLocationRelativeTo(null); // put frame at center of screen
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
                frame.initBufferStrategy();
                // Create and execute the game-loop
                GameLoop game = new GameLoop(frame);
                game.init();
                ThreadPool.execute(game);
                // and the game starts ...
            } else if (e.getSource().equals(editMap)) {
                GameMap.editMap();
            } else if (e.getSource().equals(continueButton)) {
                isCountinue = true;
                Level = 1;

                menuFrame.setVisible(false);
                SaveData saveData = new SaveData();
                saveData.readMap();
                saveData.readShots();

                new SoundPlayer(true, "Resources\\Sounds\\gameSound1.wav").execute();
                GameFrame frame = new GameFrame("Simple Ball !");
                frame.setLocationRelativeTo(null); // put frame at center of screen
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
                frame.initBufferStrategy();
                // Create and execute the game-loop
                GameLoop game = new GameLoop(frame);
                saveData.readPlayerTank();
                game.init();
                ThreadPool.execute(game);
                // and the game starts ...
            }

        }
    }

}

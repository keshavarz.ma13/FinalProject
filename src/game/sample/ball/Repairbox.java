package game.sample.ball;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Repairbox extends ShotBox {
    /**
     * Constructor
     * @param x is x location of repair box
     * @param y is y location of repair box
     */
    public Repairbox (int x , int y ){
        super(x , y);
        try {
            image =  ImageIO.read(new File("Resources\\Images\\repair.png"));
        } catch (IOException e) {

        }
    }

    /**
     * This method is for check collision of player's tank with shots
     * @param x is x location of player tank
     * @param y is y location of player tank
     */
    @Override
    public void colision(int x, int y) {
        Rectangle r = new Rectangle(x, y, 180, 180);
        Rectangle p = new Rectangle(locX , locY , 66, 40);
        if (p.intersects(r)) {
            new SoundPlayer(false, "Resources\\Sounds\\repair.wav").execute();
            GameState.lifeOfTank = 50/Menu.Level;
            isVisible = false;
        }
    }
}

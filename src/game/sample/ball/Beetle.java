package game.sample.ball;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Beetle {

    /**
     * field
     */


    public int locX, locY, diam;
    public double angle;
    BufferedImage image = null;
    public int lifeOfBeetle;
    public boolean isLife;
    public boolean isVisible;
    public static ArrayList<Beetle> beetles = new ArrayList<Beetle>();
    private int numberOfMove = 0;


    /**
     * constructor
     * @param x = x location of beetle
     * @param y = y location of beetle
     * @param imgPath = path of image of beetle
     */
    public Beetle(int x, int y, String imgPath ) {
        locX = x;
        locY = y;

        try {
            image = ImageIO.read(new File(imgPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        diam = image.getHeight();
        /**
         * set life of beetle
         */
        lifeOfBeetle = 5;
        //
        isLife = true;
        isVisible = false;


    }

    /**
     * moving find angel and change frame of moving beetle
     * @param x = x location of player tank
     * @param y = y location of player tank
     */
    public void move(int x, int y) {

        /**
         * change frame
         */
        if (numberOfMove % 5 == 0) {
            try {
                image = ImageIO.read(new File("Resources\\Images\\Enemy1.png"));
            } catch (IOException e) {

            }
        } else if (numberOfMove % 5 == 3) {
            try {
                image = ImageIO.read(new File("Resources\\Images\\Enemy2.png"));
            } catch (IOException e) {

            }
        }

        /**
         * checking life of beetle
         */
        lifeOfBeetle -= collisionShots();
        colision(x, y);

        if (lifeOfBeetle <= 0) {
            new SoundPlayer(false, "Resources\\Sounds\\enemydestroyed.wav").execute();
            isLife = false;
        }

        /**
         * find angle of beetle
         */
        if (x + 86 > locX + 50)
            angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(0);

        else if (x + 86 < locX + 50) {
            angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(180);
        }


        if ((Math.abs(locX - x) < 800 && Math.abs(locY - y) < 800) && locY>0) {
            isVisible = true;
            locY += 10 * Math.sin(angle);
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4))
                        flag = true;
                }
            }
            locY += -10 * Math.sin(angle);
            if (Math.abs(y - (locY - 10 * Math.sin(angle))) < Math.abs(y - locY)) {
                if (!flag)
                    locY -= 10 * Math.sin(angle);
            }


            locX -= -10 * Math.cos(angle);
            boolean flag2 = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4)) {
                        flag2 = true;
                    }
                }
            }
            locX += -10 * Math.cos(angle);

            if (!flag2)
                locX -= -10 * Math.cos(angle);
        }
        numberOfMove++;
    }

    /**
     * changing location
     * @param differenceX = difference of x location
     * @param differenceY = difference of y location
     */
    public void changLocation(int differenceX, int differenceY) {
        locX += differenceX;
        locY += differenceY;
    }

    /**
     * checking collision of beetle with player tank
     * @param x = x location of player tank
     * @param y = y location of player tank
     */
    public void colision(int x, int y) {
        if ((Math.abs(locX - x) < 800 && Math.abs(locY - y) < 800) && locY>0)
            isVisible = true;
        Rectangle r = new Rectangle(x, y, 180, 180);
        Rectangle p = new Rectangle(locX + 14, locY + 14, 60, 60);
        if (p.intersects(r)) {

            GameState.lifeOfTank -= 5;
            isLife = false;
            isVisible = false;
        }

    }

    /**
     * checking collision of beetle with object block
     * @param objectBlock = an object block
     * @return if collided return true else return false
     */
    public boolean colision(ObjectBlock objectBlock) {
        Rectangle r = new Rectangle(objectBlock.x, objectBlock.y, objectBlock.image.getWidth(), objectBlock.image.getHeight());
        Rectangle p = new Rectangle(locX + 14, locY + 14, 60, 60);

        // Assuming there is an intersect method, otherwise just handcompare the values
        if (p.intersects(r)) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * checking collision of beetle with shots
     * @return number of collisions
     */
    public Integer collisionShots() {
        int number = 0;
        if (Gunshot.getShots().size() <= 30) {
            for (int i = 0; i < Gunshot.getShots().size(); i++) {
                Rectangle r = new Rectangle(Gunshot.getShots().get(i).locationX, Gunshot.getShots().get(i).locationY, Gunshot.getShots().get(i).getImg().getWidth(), Gunshot.getShots().get(i).getImg().getHeight());
                Rectangle p = new Rectangle(locX + 14, locY + 14, 60, 60);

                // Assuming there is an intersect method, otherwise just handCompare the values
                if (p.intersects(r) && !Gunshot.getShots().get(i).fromEnemy) {
                    number++;
                    Gunshot.getShots().get(i).locationX = 10000;
                    Gunshot.getShots().get(i).locationY = 10000;
                }

            }
        } else {
            for (int i = Gunshot.getShots().size() - 30; i < Gunshot.getShots().size(); i++) {

                Rectangle r = new Rectangle(Gunshot.getShots().get(i).locationX, Gunshot.getShots().get(i).locationY, Gunshot.getShots().get(i).getImg().getWidth(), Gunshot.getShots().get(i).getImg().getHeight());
                Rectangle p = new Rectangle(locX + 14, locY + 14, 60, 60);

                // Assuming there is an intersect method, otherwise just handCompare the values
                if (p.intersects(r) && !Gunshot.getShots().get(i).fromEnemy) {
                    number++;
                    Gunshot.getShots().get(i).locationX = 10000;
                    Gunshot.getShots().get(i).locationY = 10000;
                }
            }
        }
        if (GameState.isMachineGun)
            return number;
        else
            return number * 5;
    }
}

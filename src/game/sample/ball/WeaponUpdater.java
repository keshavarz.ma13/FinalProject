package game.sample.ball;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class WeaponUpdater extends ShotBox {

    /**
     * Constructor
     *
     * @param x is x location of weapon
     * @param y is y location of weapon
     */
    public WeaponUpdater(int x, int y) {
        super(x, y);
        try {
            image = ImageIO.read(new File("Resources\\Images\\weaponUpdate.png"));
        } catch (IOException e) {

        }
    }

    /**
     * Check the collision of player tank with weapon
     *
     * @param x is x location of player tank
     * @param y is y location of player tank
     */
    @Override
    public void colision(int x, int y) {
        Rectangle r = new Rectangle(x, y, 180, 180);
        Rectangle p = new Rectangle(locX, locY, 52, 44);
        if (p.intersects(r)) {
            new SoundPlayer(false, "Resources\\Sounds\\repair.wav").execute();
            isVisible = false;
            GameState.weaponPower++;
        }
    }
}

package game.sample.ball;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ObjectBlock {

    /**
     * Fields
     */
    int x, y;
    BufferedImage image;
    int type;
    int softWallLevel = 20;

    /**
     * Constructor
     *
     * @param type      is for type of object blocks
     * @param x         is for x location of object block
     * @param y         is for y location of object block
     * @param imagePath if for path of object block image
     */
    public ObjectBlock(int type, int x, int y, String imagePath) {
        this.x = x;
        this.y = y;
        this.type = type;
        try {
            image = ImageIO.read(new File(imagePath));
        } catch (IOException e) {
        }
    }

    /**
     * Thie method is for set y location of object blocks
     *
     * @param y is for y location of object block
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * This method is for set x location of object blocks
     *
     * @param x is for x location of object block
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * This method is for get y location of object blocks
     *
     * @return y location
     */
    public int getY() {
        return y;
    }

    /**
     * This method is for get x location of object blocks
     *
     * @return x location
     */
    public int getX() {
        return x;
    }

    /**
     * This method is for set path of image
     *
     * @param path is path of object block image
     */
    public void setimg(String path) {
        try {
            image = ImageIO.read(new File(path));
        } catch (IOException e) {
        }
    }
}

package game.sample.ball;

import org.omg.CORBA.MARSHAL;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

public class Tank {


    /**
     * fields
     */
    public int locX, locY, diam;
    public double angle;
    boolean gameOver;
    Random rand;
    int random;
    BufferedImage img = null;
    BufferedImage gunImage = null;
    public int numberOfMove;
    public int lifeOfEnemyTank;
    public boolean isLife;
    public static ArrayList<Tank> enemyTanks = new ArrayList<Tank>();
    Tank tank;


    /**
     * constructor
     *
     * @param x        = x location of enemy tank
     * @param y        = y location of enemy tank
     * @param imgPath  = path of the body of enemy tank
     * @param imgPath2 = path of the gun of enemy tank
     */
    public Tank(int x, int y, String imgPath, String imgPath2) {
        locX = x;
        locY = y;

        try {
            img = ImageIO.read(new File(imgPath));
            gunImage = ImageIO.read(new File(imgPath2));
        } catch (IOException e) {
            e.printStackTrace();
        }
        diam = img.getHeight();
        rand = new Random();
        //
        lifeOfEnemyTank = 5;
        //
        isLife = true;


    }

    /**
     * moving the enemy tank and find angel to rotate gun of enemy tank
     *
     * @param x = x location of player tank
     * @param y = y location of player tank
     */
    public void move(int x, int y) {
        lifeOfEnemyTank -= collisionShots();
        if (lifeOfEnemyTank <= 0) {
            new SoundPlayer(false, "Resources\\Sounds\\enemydestroyed.wav").execute();
            isLife = false;
        }
        if (numberOfMove % 20 == 0) {
            if (Math.abs(locX - x) < 1000 && Math.abs(locY - y) < 1000 && locY > 0 && locY < GameFrame.GAME_HEIGHT) {
                new SoundPlayer(false, "Resources\\Sounds\\enemyshot.wav").execute();
                Gunshot shot = new Gunshot(locX + 50, locY + 50, angle * (-1), true, false);
                Gunshot.getShots().add(shot);
            }
            random = rand.nextInt(4) + 1;
        }
        //find angel
        if (x + 86 > locX + 50)
            angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(0);

        else if (x + 86 < locX + 50) {
            angle = (-1) * Math.atan((double) ((y + 92 - (locY + 50.0)) / (x + 86 - (locX + 50.0)))) + Math.toRadians(180);
        }

        if (random == 1) {
            locY -= 8;
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4))
                        flag = true;
                }
            }
            locY += 8;
            if (Math.abs(y - (locY - 8)) < Math.abs(y - locY)) {
                if (!flag)
                    locY -= 8;
            }
        }
        if (random == 2) {
            locY += 8;
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4))
                        flag = true;
                }
            }
            locY -= 8;
            if (Math.abs(y - (locY + 8)) < Math.abs(y - locY)) {
                if (!flag)
                    locY += 8;
            }
        }
        if (random == 3) {
            locX -= 8;
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4)) {
                        flag = true;
                    }
                }
            }
            locX += 8;

            if (!flag)
                locX -= 8;
        }
        if (random == 4) {
            locX += 8;
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 25; j++) {
                    if (colision(GameMap.map[i][j]) && (GameMap.map[i][j].type == 2 || GameMap.map[i][j].type == 3 || GameMap.map[i][j].type == 4)) {
                        flag = true;
                    }
                }
            }
            locX -= 8;
            if (!flag)
                locX += 8;
        }

        numberOfMove++;
    }

    /**
     * This method is for change location of player tank
     *
     * @param differenceX is for change location of x
     * @param differenceY is for change location of y
     */
    public void changLocation(int differenceX, int differenceY) {
        locX += differenceX;
        locY += differenceY;
    }

    /**
     * This method is for check collision of player's tank with object blocks
     *
     * @param objectBlock is for object block of game
     * @return true if collided and otherwise return false
     */
    public boolean colision(ObjectBlock objectBlock) {
        Rectangle r = new Rectangle(objectBlock.x, objectBlock.y, objectBlock.image.getWidth(), objectBlock.image.getHeight());
        Rectangle p = new Rectangle(locX, locY, 170, 180);

        // Assuming there is an intersect method, otherwise just handcompare the values
        if (p.intersects(r)) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * This method is for check collision of player's tank with shots
     *
     * @return
     */
    public Integer collisionShots() {
        int number = 0;
        if (Gunshot.getShots().size() <= 30) {
            for (int i = 0; i < Gunshot.getShots().size(); i++) {
                Rectangle r = new Rectangle(Gunshot.getShots().get(i).locationX, Gunshot.getShots().get(i).locationY, Gunshot.getShots().get(i).getImg().getWidth(), Gunshot.getShots().get(i).getImg().getHeight());
                Rectangle p = new Rectangle(locX, locY, 100, 100);

                // Assuming there is an intersect method, otherwise just handCompare the values
                if (p.intersects(r) && !Gunshot.getShots().get(i).fromEnemy) {
                    number++;
                    Gunshot.getShots().get(i).locationX = 10000;
                    Gunshot.getShots().get(i).locationY = 10000;
                }

            }
        } else {
            for (int i = Gunshot.getShots().size() - 30; i < Gunshot.getShots().size(); i++) {
                Rectangle r = new Rectangle(Gunshot.getShots().get(i).locationX, Gunshot.getShots().get(i).locationY, Gunshot.getShots().get(i).getImg().getWidth(), Gunshot.getShots().get(i).getImg().getHeight());
                Rectangle p = new Rectangle(locX, locY, 100, 100);

                // Assuming there is an intersect method, otherwise just handCompare the values
                if (p.intersects(r) && !Gunshot.getShots().get(i).fromEnemy) {
                    number++;
                    Gunshot.getShots().get(i).locationX = 10000;
                    Gunshot.getShots().get(i).locationY = 10000;
                }
            }
        }
        if (GameState.isMachineGun)
            return number;
        else
            return number * 5;
    }
}

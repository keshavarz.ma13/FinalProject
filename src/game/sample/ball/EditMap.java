package game.sample.ball;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * fields
 */
public class EditMap {
    JFrame mapEditor = new JFrame("Edit Map");
    ImageIcon backImage = new ImageIcon("Resources\\Images\\editMap.png");
    JLabel background = new JLabel(backImage);
    JButton save = new JButton("Save");
    JButton cancel = new JButton("Cancel");
    int i , j ;


    /**
     * constructors
     * design frame of edit map frame
     */
    public EditMap() {
        ActionHandler act = new ActionHandler();
        mapEditor.setExtendedState(JFrame.MAXIMIZED_BOTH);
        mapEditor.setUndecorated(true);
        mapEditor.setBackground(Color.BLACK);
        mapEditor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        save.setBounds(new Rectangle(1550, 800, 80, 50));
        cancel.setBounds(new Rectangle(1650, 800, 80, 50));
        mapEditor.setLayout(null);
        for ( i = 0; i < 20; i++) {
            for ( j = 0; j < 25; j++) {

                GameMap.mapLabel[i][j].setBounds(new Rectangle(100 + i * 60, 900 - j * 25, 50, 20));
                GameMap.mapLabel[i][j].addActionListener(act);
                if (GameMap.mapLabel[i][j].getText().equals("s"))
                    GameMap.mapLabel[i][j].setBackground(Color.PINK);
                else if (GameMap.mapLabel[i][j].getText().equals("p"))
                    GameMap.mapLabel[i][j].setBackground(Color.GREEN);
                else if (GameMap.mapLabel[i][j].getText().equals("hw"))
                    GameMap.mapLabel[i][j].setBackground(Color.red);
                else if (GameMap.mapLabel[i][j].getText().equals("sw"))
                    GameMap.mapLabel[i][j].setBackground(Color.gray);
                else if (GameMap.mapLabel[i][j].getText().equals("t"))
                    GameMap.mapLabel[i][j].setBackground(Color.WHITE);

                mapEditor.add(GameMap.mapLabel[i][j]);
                save.addActionListener(act);
                cancel.addActionListener(act);
                mapEditor.add(save);
                mapEditor.add(cancel);
                background.setBounds(new Rectangle(0, 0, GameFrame.GAME_WIDTH, GameFrame.GAME_HEIGHT));
                mapEditor.add(background);

            }
        }
    }

    /**
     * showing edit map frame
     */
    public void show() {
        mapEditor.setVisible(true);

    }

    /**
     * write an action listener for save and cancel button
     */
    class ActionHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(cancel)) {
                mapEditor.setVisible(false);

            } else if (e.getSource().equals(save)) {

                try (BufferedWriter bw = new BufferedWriter(new FileWriter("map.txt"))) {


                    for (int j = 0; j < 25; j++)
                        for (int i = 0; i < 20; i++) {
                            bw.write(GameMap.mapLabel[i][j].getText() + "\n");
                        }

                    mapEditor.setVisible(false);


                } catch (IOException e1) {
                }
                for (int i = 0; i < 20; i++) {
                    for (int j = 0; j < 25; j++) {
                        if(e.getSource().equals(GameMap.mapLabel[i][j])){
                            System.out.println("slm");
                            if (GameMap.mapLabel[i][j].getText().equals("s"))
                                GameMap.mapLabel[i][j].setBackground(Color.PINK);
                            else if (GameMap.mapLabel[i][j].getText().equals("p"))
                                GameMap.mapLabel[i][j].setBackground(Color.GREEN);
                            else if (GameMap.mapLabel[i][j].getText().equals("hw"))
                                GameMap.mapLabel[i][j].setBackground(Color.red);
                            else if (GameMap.mapLabel[i][j].getText().equals("sw"))
                                GameMap.mapLabel[i][j].setBackground(Color.gray);
                        }
                    }
                }

            }

        }
    }

}


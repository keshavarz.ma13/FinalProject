package game.sample.ball;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ShotBox {
    /**
     * fields
     */
    //save shots
    public static ArrayList<ShotBox> ShotBoxes = new ArrayList<>();
    //location of shot
    public int locX, locY;
    //is in game or no
    public boolean isVisible = true;

    public BufferedImage image;

    /**
     * constructor
     *
     * @param x is x location of shot box
     * @param y is y location of shot box
     */
    public ShotBox(int x, int y) {
        this.locX = x;
        this.locY = y;
        try {
            image = ImageIO.read(new File("Resources\\Images\\CannonFood2.png"));
        } catch (IOException e) {

        }
    }

    /**
     * This method is for check collision of player's tank with shots
     *
     * @param x is x location of player tank
     * @param y is y location of player tank
     */
    public void colision(int x, int y) {

        Rectangle r = new Rectangle(x, y, 180, 180);
        Rectangle p = new Rectangle(locX, locY, 52, 44);
        if (p.intersects(r)) {
            //play sound of collision
            new SoundPlayer(false, "Resources\\Sounds\\repair.wav").execute();
            GameState.limitOfShots += 45;
            isVisible = false;
        }

    }
}

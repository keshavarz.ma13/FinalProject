package game.sample.ball;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class GameOver {
    /**
     * Fielda
     */
    JFrame gameOverFrame = new JFrame("Edit Map");
    ImageIcon backImage = new ImageIcon("Resources\\Images\\gameOver.png");
    JLabel background = new JLabel(backImage);
    JButton exit = new JButton("Exit");
    JButton playAgain = new JButton("Play again");

    /**
     * Constructor
     */
    public GameOver() {

        GameOver.ActionHandler actionHandler = new GameOver.ActionHandler();
        gameOverFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        gameOverFrame.setUndecorated(true);
        gameOverFrame.setBackground(Color.BLACK);
        gameOverFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        exit.setBounds(new Rectangle(1550 - 700, 800, 80, 50));
        playAgain.setBounds(new Rectangle(1650 - 700, 800, 120, 50));
        exit.addActionListener(actionHandler);
        playAgain.addActionListener(actionHandler);
        gameOverFrame.add(exit);
        gameOverFrame.add(playAgain);
        gameOverFrame.add(background);
    }

    /**
     * This method is for show the game over frame is the ehd of game when you lost
     */
    public void show() {
        gameOverFrame.setVisible(true);
    }


    class ActionHandler implements ActionListener {

        /**
         * Action listener for buttons are in frame
         *
         * @param e is for action listener
         */
        @Override
        public void actionPerformed(ActionEvent e) {

            for (Tank tank : Tank.enemyTanks)
                tank.isLife = false;
            for (Beetle beetle : Beetle.beetles)
                beetle.isLife = false;
            for (Gunshot gunshot : Gunshot.getShots()) {
                gunshot.locationX = 10000;
            }
            for (ShotBox shotbox : ShotBox.ShotBoxes) {
                shotbox.isVisible = false;

            }
            if (GameFrame.numberOfMap == 1) {
                GameState.locY = 100;
                GameState.locX = 100;
            } else {
                GameState.locY = 200;
                GameState.locX = 200;
            }
            if (e.getSource().equals(exit)) {
                gameOverFrame.dispatchEvent(new WindowEvent(gameOverFrame, WindowEvent.WINDOW_CLOSING));
            }
            if (e.getSource().equals(playAgain)) {

                if (GameFrame.numberOfMap == 1)
                    GameMap.readMap("map.txt");
                else
                    GameMap.readMap("map2.txt");
                GameFrame frame = new GameFrame("Simple Ball !");
                frame.setLocationRelativeTo(null); // put frame at center of screen
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
                frame.initBufferStrategy();
                // Create and execute the game-loop
                GameLoop game = new GameLoop(frame);
                game.init();
                ThreadPool.execute(game);
                // and the game starts ...
            }
        }
    }

}

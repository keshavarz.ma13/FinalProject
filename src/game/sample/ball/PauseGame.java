package game.sample.ball;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;


public class PauseGame {

    /**
     * Fields
     */
    JFrame pauseFrame = new JFrame();
    JButton resume;
    JButton exit;
    ActionHandler actionHandler = new ActionHandler();

    /**
     * Constructor
     */
    public PauseGame() {
        pauseFrame.setSize(200, 150);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        pauseFrame.setUndecorated(true);
        pauseFrame.setLocation(dim.width / 2 - pauseFrame.getSize().width / 2, dim.height / 2 - pauseFrame.getSize().height / 2);
        pauseFrame.setLayout(null);
        ImageIcon backGround = new ImageIcon("Resources\\Images\\pauseBackGround.png");
        ImageIcon resumeImage = new ImageIcon("Resources\\Images\\resume.png");
        ImageIcon exitImage = new ImageIcon("Resources\\Images\\exit.png");
        resume = new JButton(resumeImage);
        exit = new JButton(exitImage);
        JLabel backGroundLabel = new JLabel(backGround);
        backGroundLabel.setBounds(new Rectangle(0, 0, 200, 150));
        resume.setBounds(new Rectangle(50 - 10, 20 + 40, 50, 50));
        exit.setBounds(new Rectangle(110 - 10, 20 + 40, 50, 50));

        pauseFrame.add(resume);
        pauseFrame.add(exit);
        pauseFrame.add(backGroundLabel);
        resume.addActionListener(actionHandler);
        exit.addActionListener(actionHandler);


    }

    /**
     * This method is for show the pause frame
     */
    public void show() {
        pauseFrame.setVisible(true);
    }

    class ActionHandler implements ActionListener {

        /**
         * This method is for action listener for click buttons
         *
         * @param e is for action listener
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(resume)) {
                GameFrame.isPaused = false;
                pauseFrame.setVisible(false);
            } else if (e.getSource().equals(exit)) {
                pauseFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                SaveData saveData = new SaveData();
                saveData.beetleSave();
                saveData.enemySave();
                saveData.saveBox();
                saveData.saveMap();
                saveData.savePlayerTank();
                saveData.saveShots();

                pauseFrame.dispatchEvent(new WindowEvent(pauseFrame, WindowEvent.WINDOW_CLOSING));

            }
        }
    }
}
